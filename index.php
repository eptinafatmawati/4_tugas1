<?php
require_once 'animal.php';
require_once 'Ape.php';
require_once 'Frog.php';

$sheep = new Animal("Shaun");
echo "Name: " . $sheep->getName() . "<br>";
echo "Legs: " . $sheep->getLegs() . "<br>";
echo "Cold blooded: " . $sheep->getColdBlooded() . "<br>";
echo "<br>";

$kodok = new Frog("Buduk");
echo "Name: " . $kodok->getName() . "<br>";
echo "Legs: " . $kodok->getLegs() . "<br>";
echo "Cold blooded: " . $kodok->getColdBlooded() . "<br>";
echo "Jump: " . $kodok->jump() . "<br>";
echo "<br>";

$sungokong = new Ape("Kera Sakti");
echo "Name: " . $sungokong->getName() . "<br>";
echo "Legs: " . $sungokong->getLegs() . "<br>";
echo "Cold blooded: " . $sungokong->getColdBlooded() . "<br>";
echo "Yell: " . $sungokong->yell() . "<br>";
?>